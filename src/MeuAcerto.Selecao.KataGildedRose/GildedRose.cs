﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        private void qualityDecrement(Item item)
        {
            if(item.Qualidade > 0)
            {
                if (item.Nome.Contains("Conjurado"))
                {
                    item.Qualidade = item.Qualidade >= 2 ? item.Qualidade - 2 : item.Qualidade - 1;
                }
                else
                {
                    item.Qualidade --;
                }
            }
            
        }

        private bool isEspecial(Item item)
        {
            return (item.Nome == "Queijo Brie Envelhecido" || item.Nome == "Ingressos para o concerto do Turisas" || item.Nome == "Dente do Tarrasque");
        }

        private bool qualityIsValid(int quality)
        {
            return quality < 50;
        }


        public void AtualizarQualidade()
        {
            foreach (Item item in Itens)
            {
                if (!isEspecial(item))
                {
                    qualityDecrement(item);
                }
                else if (qualityIsValid(item.Qualidade))
                {
                    item.Qualidade++;

                    if (item.Nome == "Ingressos para o concerto do Turisas" && qualityIsValid(item.Qualidade))
                    {
                        if (item.PrazoParaVenda <= 10)
                        {
                            item.Qualidade ++;
                        }

                        if (item.PrazoParaVenda <= 5)
                        {

                            item.Qualidade ++;

                        }
                    }
                }

                if (item.Nome != "Dente do Tarrasque")
                {
                    item.PrazoParaVenda--;
                }

                if (item.PrazoParaVenda < 0)
                {
                    if (item.Nome != "Queijo Brie Envelhecido")
                    {
                        if (item.Nome != "Ingressos para o concerto do Turisas" && item.Nome != "Dente do Tarrasque")
                        {
                            qualityDecrement(item);
                        }
                        else
                        {
                            item.Qualidade = item.Qualidade - item.Qualidade;
                        }
                    }
                    else if (qualityIsValid(item.Qualidade))
                    {
                        item.Qualidade++;
                    }
                }
            }

        }
    }
}
